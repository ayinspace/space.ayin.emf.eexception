/**
 * generated by Xtext 2.16.0
 */
package space.ayin.lang.ayin;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link space.ayin.lang.ayin.AyinEvent#getName <em>Name</em>}</li>
 *   <li>{@link space.ayin.lang.ayin.AyinEvent#getParamStruct <em>Param Struct</em>}</li>
 * </ul>
 *
 * @see space.ayin.lang.ayin.AyinPackage#getAyinEvent()
 * @model
 * @generated
 */
public interface AyinEvent extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see space.ayin.lang.ayin.AyinPackage#getAyinEvent_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link space.ayin.lang.ayin.AyinEvent#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Param Struct</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param Struct</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param Struct</em>' containment reference.
   * @see #setParamStruct(AyinStruct)
   * @see space.ayin.lang.ayin.AyinPackage#getAyinEvent_ParamStruct()
   * @model containment="true"
   * @generated
   */
  AyinStruct getParamStruct();

  /**
   * Sets the value of the '{@link space.ayin.lang.ayin.AyinEvent#getParamStruct <em>Param Struct</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param Struct</em>' containment reference.
   * @see #getParamStruct()
   * @generated
   */
  void setParamStruct(AyinStruct value);

} // AyinEvent
