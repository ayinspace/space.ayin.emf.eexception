/**
 * generated by Xtext 2.16.0
 */
package space.ayin.lang.ayin.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import space.ayin.lang.ayin.AyinEvent;
import space.ayin.lang.ayin.AyinPackage;
import space.ayin.lang.ayin.AyinStruct;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link space.ayin.lang.ayin.impl.AyinEventImpl#getName <em>Name</em>}</li>
 *   <li>{@link space.ayin.lang.ayin.impl.AyinEventImpl#getParamStruct <em>Param Struct</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AyinEventImpl extends MinimalEObjectImpl.Container implements AyinEvent
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getParamStruct() <em>Param Struct</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParamStruct()
   * @generated
   * @ordered
   */
  protected AyinStruct paramStruct;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AyinEventImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AyinPackage.Literals.AYIN_EVENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AyinPackage.AYIN_EVENT__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AyinStruct getParamStruct()
  {
    return paramStruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParamStruct(AyinStruct newParamStruct, NotificationChain msgs)
  {
    AyinStruct oldParamStruct = paramStruct;
    paramStruct = newParamStruct;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AyinPackage.AYIN_EVENT__PARAM_STRUCT, oldParamStruct, newParamStruct);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParamStruct(AyinStruct newParamStruct)
  {
    if (newParamStruct != paramStruct)
    {
      NotificationChain msgs = null;
      if (paramStruct != null)
        msgs = ((InternalEObject)paramStruct).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AyinPackage.AYIN_EVENT__PARAM_STRUCT, null, msgs);
      if (newParamStruct != null)
        msgs = ((InternalEObject)newParamStruct).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AyinPackage.AYIN_EVENT__PARAM_STRUCT, null, msgs);
      msgs = basicSetParamStruct(newParamStruct, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AyinPackage.AYIN_EVENT__PARAM_STRUCT, newParamStruct, newParamStruct));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EVENT__PARAM_STRUCT:
        return basicSetParamStruct(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EVENT__NAME:
        return getName();
      case AyinPackage.AYIN_EVENT__PARAM_STRUCT:
        return getParamStruct();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EVENT__NAME:
        setName((String)newValue);
        return;
      case AyinPackage.AYIN_EVENT__PARAM_STRUCT:
        setParamStruct((AyinStruct)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EVENT__NAME:
        setName(NAME_EDEFAULT);
        return;
      case AyinPackage.AYIN_EVENT__PARAM_STRUCT:
        setParamStruct((AyinStruct)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EVENT__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case AyinPackage.AYIN_EVENT__PARAM_STRUCT:
        return paramStruct != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //AyinEventImpl
