/**
 * generated by Xtext 2.16.0
 */
package space.ayin.lang.ayin.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import space.ayin.lang.ayin.AyinEventHandler;
import space.ayin.lang.ayin.AyinExecutor;
import space.ayin.lang.ayin.AyinInterface;
import space.ayin.lang.ayin.AyinPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Executor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link space.ayin.lang.ayin.impl.AyinExecutorImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link space.ayin.lang.ayin.impl.AyinExecutorImpl#getHandlers <em>Handlers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AyinExecutorImpl extends AyinParentableImpl implements AyinExecutor
{
  /**
   * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInterfaces()
   * @generated
   * @ordered
   */
  protected EList<AyinInterface> interfaces;

  /**
   * The cached value of the '{@link #getHandlers() <em>Handlers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHandlers()
   * @generated
   * @ordered
   */
  protected EList<AyinEventHandler> handlers;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AyinExecutorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AyinPackage.Literals.AYIN_EXECUTOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AyinInterface> getInterfaces()
  {
    if (interfaces == null)
    {
      interfaces = new EObjectResolvingEList<AyinInterface>(AyinInterface.class, this, AyinPackage.AYIN_EXECUTOR__INTERFACES);
    }
    return interfaces;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AyinEventHandler> getHandlers()
  {
    if (handlers == null)
    {
      handlers = new EObjectContainmentEList<AyinEventHandler>(AyinEventHandler.class, this, AyinPackage.AYIN_EXECUTOR__HANDLERS);
    }
    return handlers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EXECUTOR__HANDLERS:
        return ((InternalEList<?>)getHandlers()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EXECUTOR__INTERFACES:
        return getInterfaces();
      case AyinPackage.AYIN_EXECUTOR__HANDLERS:
        return getHandlers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EXECUTOR__INTERFACES:
        getInterfaces().clear();
        getInterfaces().addAll((Collection<? extends AyinInterface>)newValue);
        return;
      case AyinPackage.AYIN_EXECUTOR__HANDLERS:
        getHandlers().clear();
        getHandlers().addAll((Collection<? extends AyinEventHandler>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EXECUTOR__INTERFACES:
        getInterfaces().clear();
        return;
      case AyinPackage.AYIN_EXECUTOR__HANDLERS:
        getHandlers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AyinPackage.AYIN_EXECUTOR__INTERFACES:
        return interfaces != null && !interfaces.isEmpty();
      case AyinPackage.AYIN_EXECUTOR__HANDLERS:
        return handlers != null && !handlers.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //AyinExecutorImpl
