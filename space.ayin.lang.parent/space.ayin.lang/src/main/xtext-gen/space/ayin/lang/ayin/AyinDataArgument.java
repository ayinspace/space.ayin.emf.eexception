/**
 * generated by Xtext 2.16.0
 */
package space.ayin.lang.ayin;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link space.ayin.lang.ayin.AyinDataArgument#isIn <em>In</em>}</li>
 *   <li>{@link space.ayin.lang.ayin.AyinDataArgument#isOut <em>Out</em>}</li>
 *   <li>{@link space.ayin.lang.ayin.AyinDataArgument#getArgName <em>Arg Name</em>}</li>
 * </ul>
 *
 * @see space.ayin.lang.ayin.AyinPackage#getAyinDataArgument()
 * @model
 * @generated
 */
public interface AyinDataArgument extends EObject
{
  /**
   * Returns the value of the '<em><b>In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>In</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>In</em>' attribute.
   * @see #setIn(boolean)
   * @see space.ayin.lang.ayin.AyinPackage#getAyinDataArgument_In()
   * @model
   * @generated
   */
  boolean isIn();

  /**
   * Sets the value of the '{@link space.ayin.lang.ayin.AyinDataArgument#isIn <em>In</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>In</em>' attribute.
   * @see #isIn()
   * @generated
   */
  void setIn(boolean value);

  /**
   * Returns the value of the '<em><b>Out</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Out</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Out</em>' attribute.
   * @see #setOut(boolean)
   * @see space.ayin.lang.ayin.AyinPackage#getAyinDataArgument_Out()
   * @model
   * @generated
   */
  boolean isOut();

  /**
   * Sets the value of the '{@link space.ayin.lang.ayin.AyinDataArgument#isOut <em>Out</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Out</em>' attribute.
   * @see #isOut()
   * @generated
   */
  void setOut(boolean value);

  /**
   * Returns the value of the '<em><b>Arg Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arg Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg Name</em>' attribute.
   * @see #setArgName(String)
   * @see space.ayin.lang.ayin.AyinPackage#getAyinDataArgument_ArgName()
   * @model
   * @generated
   */
  String getArgName();

  /**
   * Sets the value of the '{@link space.ayin.lang.ayin.AyinDataArgument#getArgName <em>Arg Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg Name</em>' attribute.
   * @see #getArgName()
   * @generated
   */
  void setArgName(String value);

} // AyinDataArgument
