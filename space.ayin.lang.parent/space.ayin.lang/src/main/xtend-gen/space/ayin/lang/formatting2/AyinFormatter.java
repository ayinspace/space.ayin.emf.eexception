/**
 * generated by Xtext 2.11.0
 */
package space.ayin.lang.formatting2;

import java.util.Arrays;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.formatting2.AbstractFormatter2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.formatting2.IHiddenRegionFormatter;
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import space.ayin.lang.ayin.AyinAssignment;
import space.ayin.lang.ayin.AyinBlock;
import space.ayin.lang.ayin.AyinEnum;
import space.ayin.lang.ayin.AyinEvent;
import space.ayin.lang.ayin.AyinEventHandler;
import space.ayin.lang.ayin.AyinExecutor;
import space.ayin.lang.ayin.AyinExpression;
import space.ayin.lang.ayin.AyinInterface;
import space.ayin.lang.ayin.AyinMethod;
import space.ayin.lang.ayin.AyinMethodCall;
import space.ayin.lang.ayin.AyinPackage;
import space.ayin.lang.ayin.AyinStruct;

@SuppressWarnings("all")
public class AyinFormatter extends AbstractFormatter2 {
  protected void _format(final AyinStruct ayinStruct, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(ayinStruct).feature(AyinPackage.eINSTANCE.getAyinEntity_Name()), _function);
    final Consumer<ISemanticRegion> _function_1 = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it_1) -> {
        it_1.newLine();
      };
      document.append(it, _function_2);
    };
    this.textRegionExtensions.allRegionsFor(ayinStruct).keywords(";").forEach(_function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(
      document.append(this.textRegionExtensions.regionFor(ayinStruct).keyword("{"), _function_2), 
      document.prepend(this.textRegionExtensions.regionFor(ayinStruct).keyword("}"), _function_3), _function_4);
  }
  
  protected void _format(final AyinEnum ayinEnum, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(ayinEnum).feature(AyinPackage.eINSTANCE.getAyinEntity_Name()), _function);
    final Consumer<ISemanticRegion> _function_1 = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it_1) -> {
        it_1.newLine();
      };
      document.append(it, _function_2);
    };
    this.textRegionExtensions.allRegionsFor(ayinEnum).keywords(";").forEach(_function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(
      document.append(this.textRegionExtensions.regionFor(ayinEnum).keyword("{"), _function_2), 
      document.prepend(this.textRegionExtensions.regionFor(ayinEnum).keyword("}"), _function_3), _function_4);
  }
  
  protected void _format(final AyinInterface ayinInterface, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(ayinInterface).feature(AyinPackage.eINSTANCE.getAyinEntity_Name()), _function);
    final Consumer<ISemanticRegion> _function_1 = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it_1) -> {
        it_1.newLine();
      };
      document.append(it, _function_2);
    };
    this.textRegionExtensions.allRegionsFor(ayinInterface).keywords(";").forEach(_function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(
      document.append(this.textRegionExtensions.regionFor(ayinInterface).keyword("{"), _function_2), 
      document.prepend(this.textRegionExtensions.regionFor(ayinInterface).keyword("}"), _function_3), _function_4);
    EList<AyinEvent> _events = ayinInterface.getEvents();
    for (final AyinEvent event : _events) {
      this.format(event, document);
    }
    EList<AyinMethod> _methods = ayinInterface.getMethods();
    for (final AyinMethod method : _methods) {
      this.format(method, document);
    }
  }
  
  protected void _format(final AyinEvent event, @Extension final IFormattableDocument document) {
    final Consumer<ISemanticRegion> _function = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it_1) -> {
        it_1.oneSpace();
      };
      document.append(it, _function_1);
    };
    this.textRegionExtensions.allRegionsFor(event.getParamStruct()).keywords(",").forEach(_function);
  }
  
  protected void _format(final AyinMethod method, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(this.textRegionExtensions.regionFor(method).feature(AyinPackage.eINSTANCE.getAyinMethod_Name()), _function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(this.textRegionExtensions.regionFor(method).keyword(")"), _function_1);
    final Consumer<ISemanticRegion> _function_2 = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it_1) -> {
        it_1.oneSpace();
      };
      document.append(it, _function_3);
    };
    this.textRegionExtensions.allRegionsFor(method.getResultStruct()).keywords(",").forEach(_function_2);
    final Consumer<ISemanticRegion> _function_3 = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it_1) -> {
        it_1.oneSpace();
      };
      document.append(it, _function_4);
    };
    this.textRegionExtensions.allRegionsFor(method.getParamStruct()).keywords(",").forEach(_function_3);
    final AyinBlock body = method.getBody();
    if ((body != null)) {
      this.format(body, document);
    }
  }
  
  protected void _format(final AyinMethodCall methodCall, @Extension final IFormattableDocument document) {
    final Consumer<ISemanticRegion> _function = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it_1) -> {
        it_1.oneSpace();
      };
      document.append(it, _function_1);
    };
    this.textRegionExtensions.allRegionsFor(methodCall).keywords(",").forEach(_function);
    final EList<AyinAssignment> assignments = methodCall.getAssignments();
    for (final AyinAssignment assignment : assignments) {
      {
        final Consumer<ISemanticRegion> _function_1 = (ISemanticRegion it) -> {
          final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it_1) -> {
            it_1.oneSpace();
          };
          document.append(it, _function_2);
        };
        this.textRegionExtensions.allRegionsFor(assignment).keywords(",").forEach(_function_1);
        final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
          it.oneSpace();
        };
        document.surround(this.textRegionExtensions.regionFor(assignment).keyword("="), _function_2);
      }
    }
  }
  
  protected void _format(final AyinExecutor executor, @Extension final IFormattableDocument document) {
    final Consumer<ISemanticRegion> _function = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it_1) -> {
        it_1.oneSpace();
      };
      document.append(it, _function_1);
    };
    this.textRegionExtensions.allRegionsFor(executor).keywords(",").forEach(_function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(this.textRegionExtensions.regionFor(executor).keyword("{"), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(
      document.append(this.textRegionExtensions.regionFor(executor).keyword("{"), _function_2), 
      document.prepend(this.textRegionExtensions.regionFor(executor).keyword("}"), _function_3), _function_4);
    EList<AyinEventHandler> _handlers = executor.getHandlers();
    for (final AyinEventHandler handler : _handlers) {
      this.format(handler, document);
    }
  }
  
  protected void _format(final AyinEventHandler eventHandler, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(eventHandler).keyword("="), _function);
    this.format(eventHandler.getBlock(), document);
  }
  
  protected void _format(final AyinAssignment assignment, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(assignment).keyword("="), _function);
    final Consumer<ISemanticRegion> _function_1 = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it_1) -> {
        it_1.oneSpace();
      };
      document.append(it, _function_2);
    };
    this.textRegionExtensions.allRegionsFor(assignment).keywords(",").forEach(_function_1);
    this.format(assignment.getRight(), document);
  }
  
  protected void _format(final AyinBlock block, @Extension final IFormattableDocument document) {
    final Consumer<ISemanticRegion> _function = (ISemanticRegion it) -> {
      final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it_1) -> {
        it_1.newLine();
      };
      document.append(it, _function_1);
    };
    this.textRegionExtensions.allRegionsFor(block).keywords(";").forEach(_function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(
      document.append(this.textRegionExtensions.regionFor(block).keyword("{"), _function_1), 
      document.prepend(this.textRegionExtensions.regionFor(block).keyword("}"), _function_2), _function_3);
    EList<AyinExpression> _expressions = block.getExpressions();
    for (final AyinExpression expression : _expressions) {
      this.format(expression, document);
    }
  }
  
  public void format(final Object ayinStruct, final IFormattableDocument document) {
    if (ayinStruct instanceof AyinStruct) {
      _format((AyinStruct)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof XtextResource) {
      _format((XtextResource)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinEnum) {
      _format((AyinEnum)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinExecutor) {
      _format((AyinExecutor)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinAssignment) {
      _format((AyinAssignment)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinBlock) {
      _format((AyinBlock)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinInterface) {
      _format((AyinInterface)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinMethodCall) {
      _format((AyinMethodCall)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinEvent) {
      _format((AyinEvent)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinEventHandler) {
      _format((AyinEventHandler)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof AyinMethod) {
      _format((AyinMethod)ayinStruct, document);
      return;
    } else if (ayinStruct instanceof EObject) {
      _format((EObject)ayinStruct, document);
      return;
    } else if (ayinStruct == null) {
      _format((Void)null, document);
      return;
    } else if (ayinStruct != null) {
      _format(ayinStruct, document);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(ayinStruct, document).toString());
    }
  }
}
