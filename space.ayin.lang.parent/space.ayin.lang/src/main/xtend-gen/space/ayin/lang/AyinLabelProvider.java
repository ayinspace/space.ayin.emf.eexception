package space.ayin.lang;

import java.util.Arrays;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import space.ayin.lang.ayin.AyinDataType;
import space.ayin.lang.ayin.AyinEntity;
import space.ayin.lang.ayin.AyinEntityType;
import space.ayin.lang.ayin.AyinPrimitiveType;
import space.ayin.lang.ayin.AyinType;

@SuppressWarnings("all")
public class AyinLabelProvider {
  protected String _text(final AyinPrimitiveType type) {
    return type.getName();
  }
  
  protected String _text(final AyinEntityType type) {
    return this.text(type.getEntity());
  }
  
  protected String _text(final AyinDataType type) {
    String _xblockexpression = null;
    {
      final Function1<AyinType, String> _function = (AyinType it) -> {
        return this.text(it);
      };
      final String args = IterableExtensions.join(ListExtensions.<AyinType, String>map(type.getArguments(), _function), ",");
      String _name = type.getDatatype().getName();
      String _plus = (_name + "<");
      String _plus_1 = (_plus + args);
      _xblockexpression = (_plus_1 + ">");
    }
    return _xblockexpression;
  }
  
  protected String _text(final AyinEntity type) {
    return type.getName();
  }
  
  protected String _text(final AyinType type) {
    return type.getName();
  }
  
  protected String _text(final Void type) {
    return "NULL";
  }
  
  public String text(final EObject type) {
    if (type instanceof AyinDataType) {
      return _text((AyinDataType)type);
    } else if (type instanceof AyinEntityType) {
      return _text((AyinEntityType)type);
    } else if (type instanceof AyinPrimitiveType) {
      return _text((AyinPrimitiveType)type);
    } else if (type instanceof AyinEntity) {
      return _text((AyinEntity)type);
    } else if (type instanceof AyinType) {
      return _text((AyinType)type);
    } else if (type == null) {
      return _text((Void)null);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type).toString());
    }
  }
}
