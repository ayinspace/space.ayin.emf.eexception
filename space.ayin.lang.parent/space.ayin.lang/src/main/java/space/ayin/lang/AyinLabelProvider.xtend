package space.ayin.lang

import space.ayin.lang.ayin.AyinDataType
import space.ayin.lang.ayin.AyinEntity
import space.ayin.lang.ayin.AyinEntityType
import space.ayin.lang.ayin.AyinPrimitiveType
import space.ayin.lang.ayin.AyinType

class AyinLabelProvider {

	dispatch def String text(AyinPrimitiveType type) {
		type.name
	}
	
	dispatch def String text(AyinEntityType type) {
		type.entity.text
	}
	
	
	dispatch def String text(AyinDataType type) {
		val args = type.arguments.map[text].join(',')
		type.datatype.name + "<" + args + ">"
	}
	
	dispatch def String text(AyinEntity type) {
		type.name
	}
	
	dispatch def String text(AyinType type) {
		type.name
	}
	
	dispatch def String text(Void type) {
		"NULL"
	}
}
