package space.ayin.lang

import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.EObject
import space.ayin.lang.ayin.AyinBlock
import space.ayin.lang.ayin.AyinBooleanValue
import space.ayin.lang.ayin.AyinCallback
import space.ayin.lang.ayin.AyinData
import space.ayin.lang.ayin.AyinDataType
import space.ayin.lang.ayin.AyinDataTypeInstantion
import space.ayin.lang.ayin.AyinDoubleValue
import space.ayin.lang.ayin.AyinEntityType
import space.ayin.lang.ayin.AyinEnum
import space.ayin.lang.ayin.AyinExpression
import space.ayin.lang.ayin.AyinFactory
import space.ayin.lang.ayin.AyinFieldGet
import space.ayin.lang.ayin.AyinIntegerValue
import space.ayin.lang.ayin.AyinMethodCall
import space.ayin.lang.ayin.AyinParameter
import space.ayin.lang.ayin.AyinParentable
import space.ayin.lang.ayin.AyinPrimitiveType
import space.ayin.lang.ayin.AyinRef
import space.ayin.lang.ayin.AyinStringValue
import space.ayin.lang.ayin.AyinStructInstantion
import space.ayin.lang.ayin.AyinType
import space.ayin.lang.ayin.AyinUnderscore
import space.ayin.lang.ayin.AyinWildcard

import static extension space.ayin.lang.AyinModelUtil.*

class AyinTypeProvider {

	public static val i8Type = AyinFactory.eINSTANCE.createAyinPrimitiveType => [name = "i8"]
	public static val i16Type = AyinFactory.eINSTANCE.createAyinPrimitiveType => [name = "i16"]
	public static val i32Type = AyinFactory.eINSTANCE.createAyinPrimitiveType => [name = "i32"]
	public static val i64Type = AyinFactory.eINSTANCE.createAyinPrimitiveType => [name = "i64"]
	public static val doubleType = AyinFactory.eINSTANCE.createAyinPrimitiveType => [name = "double"]
	public static val stringType = AyinFactory.eINSTANCE.createAyinPrimitiveType => [name = "string"]
	public static val booleanType = AyinFactory.eINSTANCE.createAyinPrimitiveType => [name = "boolean"]
	public static val voidType = AyinFactory.eINSTANCE.createAyinType => [name = "void"]
	public static val underscoreType = AyinFactory.eINSTANCE.createAyinType => [name = "underscore"]
	public static val callbackType = AyinFactory.eINSTANCE.createAyinType => [name = "callback"]
	public static val blockType = AyinFactory.eINSTANCE.createAyinType => [name = "block"]

	def List<AyinType> typeFor(AyinExpression e) {
		val res = new ArrayList<AyinType>
		switch (e) {
			AyinDoubleValue:
				res.add(doubleType)
			AyinStringValue:
				res.add(stringType)
			AyinBooleanValue:
				res.add(booleanType)
			AyinIntegerValue: {
				switch value : e.value {
					case (value <= Byte.MAX_VALUE) && (value >= Byte.MIN_VALUE):
						res.add(i8Type)
					case (value <= Short.MAX_VALUE) && (value >= Short.MIN_VALUE):
						res.add(i16Type)
					case (value <= Integer.MAX_VALUE) && (value >= Integer.MIN_VALUE):
						res.add(i32Type)
					case (value <= Long.MAX_VALUE) && (value >= Long.MIN_VALUE):
						res.add(i64Type)
				}
			}
			AyinFieldGet: {
				val receiver = e.receiver
				if (receiver instanceof AyinRef) {
					if (receiver.ref instanceof AyinEnum) {
						res.add(
							AyinFactory.eINSTANCE.createAyinEntityType => [entity = receiver.ref]
						)
					} else {
						res.add(e.field.type)
					}
				} else {
					res.add(e.field.type)
				}
			}
			AyinParameter:
				res.add(e.type)
			AyinRef: {
				if (e.ref instanceof AyinCallback) {
					res.add(callbackType)
				} else {
					res.add(e.ref.type)
				}
			}
			AyinMethodCall: {
				if (e.method.resultStruct !== null) {
					res.addAll(e.method.resultStruct.fields.map[type])
				} else if (e.method.retType !== null) {
					res.add(e.method.retType)
				} else {
					res.add(voidType)
				}
			}
			AyinStructInstantion: {
				res.add(AyinFactory.eINSTANCE.createAyinEntityType => [entity = e.entity])
			}
			AyinDataTypeInstantion: {
				val dataType = AyinFactory.eINSTANCE.createAyinDataType => [datatype = e.entity as AyinData];
				dataType.arguments.addAll(e.arguments)
				res.add(dataType)
			}
			AyinUnderscore:
				res.add(underscoreType)
			AyinBlock:
				res.add(blockType)
			default:
				res.add(voidType)
		}
		return res
	}

	def dispatch boolean isConformant(Void actualType, Void expectedType) {
		true
	}

	def dispatch boolean isConformant(Void actualType, EObject expectedType) {
		true
	}
	
	def dispatch boolean isConformant(EObject actualType, Void expectedType) {
		true
	}

	def dispatch boolean isConformant(EObject actualType, AyinWildcard expectedType) {
		true
	}

	def dispatch boolean isConformant(AyinDataType actualType, AyinDataType expectedType) {
		var argumentCounter = 0
		var res = (actualType.datatype == expectedType.datatype)
		val actualArguments = actualType.arguments
		val expectedArguments = expectedType.arguments
		
		if (actualArguments.size == expectedArguments.size) {
			for (args : actualArguments) {
				var actualArg = actualArguments.get(argumentCounter)
				var expectedArg = expectedArguments.get(argumentCounter)
				res = res && isConformant(actualArg,expectedArg)
				argumentCounter++
			}
		} else {
			return false
		}
		
		return res
	}

	def dispatch boolean isConformant(AyinEntityType actualType, AyinEntityType expectedType) {
		if (expectedType.entity instanceof AyinParentable) {
			val hier = (actualType.entity as AyinParentable).parentHierarchy
			if (hier.contains(expectedType.entity)) {
				return true
			}
		}
		actualType.entity == expectedType.entity
	}

	def dispatch boolean isConformant(AyinPrimitiveType actualType, AyinPrimitiveType expectedType) {
		val actual = actualType.name
		val expected = expectedType.name

		if (actual == "i8" && #["i8", "i16", "i32", "i64"].contains(expected)) {
			return true
		}
		if (actual == "i16" && #["i16", "i32", "i64"].contains(expected)) {
			return true
		}
		if (actual == "i32" && #["i32", "i64"].contains(expected)) {
			return true
		}
		
		return (actual == expected)
	}


	def dispatch boolean isConformant(AyinType actual, AyinType expected) {
		if (expected == underscoreType) {
			return true
		}
		
		if (expected == callbackType && actual == blockType) {
			return true
		}
		
		actual == expected
	}
	
	def static dispatch boolean isEquals(AyinDataType type1, AyinDataType type2) {
		type1.datatype.name == type2.datatype.name 
	}
	
	def static dispatch boolean isEquals(AyinEntityType type1, AyinEntityType type2) {
		type1.entity == type2.entity
	}
	
	def static dispatch boolean isEquals(AyinPrimitiveType type1, AyinPrimitiveType type2) {
		type1.name == type2.name
	}
	
	def static dispatch boolean isEquals(AyinType type1, AyinType type2) {
		type1 == type2
	}
}
