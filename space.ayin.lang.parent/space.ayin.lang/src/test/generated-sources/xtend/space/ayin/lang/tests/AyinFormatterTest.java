package space.ayin.lang.tests;

import com.google.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.formatting2.FormatterRequest;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.testing.formatter.FormatterTestHelper;
import org.eclipse.xtext.testing.formatter.FormatterTestRequest;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Test;
import org.junit.runner.RunWith;
import space.ayin.lang.tests.AyinInjectorProvider;

@RunWith(XtextRunner.class)
@InjectWith(AyinInjectorProvider.class)
@SuppressWarnings("all")
public class AyinFormatterTest {
  @Inject
  @Extension
  private FormatterTestHelper _formatterTestHelper;
  
  @Test
  public void structFormat() {
    final FormatterTestRequest testReq = new FormatterTestRequest();
    FormatterRequest _formatterRequest = new FormatterRequest();
    testReq.setRequest(_formatterRequest);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("struct S{i32 x;i32 y;}");
    testReq.setToBeFormatted(_builder);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("struct S {");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("i32 x;");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("i32 y;");
    _builder_1.newLine();
    _builder_1.append("}");
    testReq.setExpectation(_builder_1);
    this._formatterTestHelper.assertFormatted(testReq);
  }
  
  @Test
  public void enumFormat() {
    final FormatterTestRequest testReq = new FormatterTestRequest();
    FormatterRequest _formatterRequest = new FormatterRequest();
    testReq.setRequest(_formatterRequest);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum E{E1;E2;E3;}");
    testReq.setToBeFormatted(_builder);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("enum E {");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("E1;");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("E2;");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("E3;");
    _builder_1.newLine();
    _builder_1.append("}");
    testReq.setExpectation(_builder_1);
    this._formatterTestHelper.assertFormatted(testReq);
  }
  
  @Test
  public void interfaceFormat() {
    final FormatterTestRequest testReq = new FormatterTestRequest();
    FormatterRequest _formatterRequest = new FormatterRequest();
    testReq.setRequest(_formatterRequest);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("interface Base { event onStart(i32 x,i32 y); ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("(i32 x,string msg)get(i64 y,i64 z){this.get(y=0,z=1);}}");
    testReq.setToBeFormatted(_builder);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("interface Base {");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("event onStart(i32 x, i32 y);");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("(i32 x, string msg) get(i64 y, i64 z) {");
    _builder_1.newLine();
    _builder_1.append("\t\t");
    _builder_1.append("this.get(y = 0, z = 1);");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("}");
    _builder_1.newLine();
    _builder_1.append("}");
    testReq.setExpectation(_builder_1);
    this._formatterTestHelper.assertFormatted(testReq);
  }
  
  @Test
  public void executorFormat() {
    final FormatterTestRequest testReq = new FormatterTestRequest();
    FormatterRequest _formatterRequest = new FormatterRequest();
    testReq.setRequest(_formatterRequest);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("executor Executor implements BaseEvents,BaseActions{onStart={i32 a=0;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("this.divideAndLog(x,y=this.divideAndLog(),message=\"test\");i32 s,a=this.divideAndLog(x=a,y=0,message=\"test\");}}");
    final String mess = _builder.toString();
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("executor Executor implements BaseEvents, BaseActions {");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("onStart = {");
    _builder_1.newLine();
    _builder_1.append("\t\t");
    _builder_1.append("i32 a = 0;");
    _builder_1.newLine();
    _builder_1.append("\t\t");
    _builder_1.append("this.divideAndLog(x, y = this.divideAndLog(), message = \"test\");");
    _builder_1.newLine();
    _builder_1.append("\t\t");
    _builder_1.append("i32 s, a = this.divideAndLog(x = a, y = 0, message = \"test\");");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("}");
    _builder_1.newLine();
    _builder_1.append("}");
    final String clean = _builder_1.toString();
    testReq.setToBeFormatted(mess);
    testReq.setExpectation(clean);
    this._formatterTestHelper.assertFormatted(testReq);
  }
  
  @Test
  public void multiAssignFormat() {
    final FormatterTestRequest testReq = new FormatterTestRequest();
    FormatterRequest _formatterRequest = new FormatterRequest();
    testReq.setRequest(_formatterRequest);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("executor A{onStart={i32 x;x=this.test();}}");
    testReq.setToBeFormatted(_builder);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("executor A {");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("onStart = {");
    _builder_1.newLine();
    _builder_1.append("\t\t");
    _builder_1.append("i32 x;");
    _builder_1.newLine();
    _builder_1.append("\t\t");
    _builder_1.append("x = this.test();");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("}");
    _builder_1.newLine();
    _builder_1.append("}");
    testReq.setExpectation(_builder_1);
    this._formatterTestHelper.assertFormatted(testReq);
  }
}
