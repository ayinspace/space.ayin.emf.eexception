package space.ayin.lang.tests;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Test;
import org.junit.runner.RunWith;
import space.ayin.lang.ayin.AyinEntity;
import space.ayin.lang.ayin.AyinExecutor;
import space.ayin.lang.ayin.AyinPackage;
import space.ayin.lang.tests.AyinInjectorProvider;
import space.ayin.lang.validation.AyinValidator;

@RunWith(XtextRunner.class)
@InjectWith(AyinInjectorProvider.class)
@SuppressWarnings("all")
public class AyinTypeProviderTest {
  @Inject
  @Extension
  private ParseHelper<AyinEntity> _parseHelper;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Inject
  private Provider<ResourceSet> resourceSetProvider;
  
  private AyinExecutor parseExecutor(final CharSequence testExp) {
    try {
      final ResourceSet resourceSet = this.resourceSetProvider.get();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("struct Pin { i8 p; }");
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder, resourceSet));
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("struct Nip { i8 p; }");
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder_1, resourceSet));
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("struct SuperPin extends Pin { string message; }");
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder_2, resourceSet));
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("exception Exception { i32 code; }");
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder_3, resourceSet));
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append("datatype List<T>");
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder_4, resourceSet));
      StringConcatenation _builder_5 = new StringConcatenation();
      _builder_5.append("exception SimpleException { i32 code; }");
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder_5, resourceSet));
      StringConcatenation _builder_6 = new StringConcatenation();
      _builder_6.append("exception ComplexException extends SimpleException { string message; }");
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder_6, resourceSet));
      StringConcatenation _builder_7 = new StringConcatenation();
      _builder_7.append("interface IBase {");
      _builder_7.newLine();
      _builder_7.append("\t");
      _builder_7.append("event onStart();");
      _builder_7.newLine();
      _builder_7.append("\t");
      _builder_7.append("void test(string msg, ");
      _builder_7.newLine();
      _builder_7.append("\t\t\t\t");
      _builder_7.append("callback test1(i8 code));");
      _builder_7.newLine();
      _builder_7.append("\t");
      _builder_7.append("void process(List<?> list);");
      _builder_7.newLine();
      _builder_7.append("}");
      _builder_7.newLine();
      this._validationTestHelper.assertNoErrors(this._parseHelper.parse(_builder_7, resourceSet));
      StringConcatenation _builder_8 = new StringConcatenation();
      _builder_8.append("executor Executor implements IBase {");
      _builder_8.newLine();
      _builder_8.append("\t");
      _builder_8.append("onStart = {");
      _builder_8.newLine();
      _builder_8.append("\t\t");
      _builder_8.append(testExp, "\t\t");
      _builder_8.newLineIfNotEmpty();
      _builder_8.append("\t");
      _builder_8.append("}");
      _builder_8.newLine();
      _builder_8.append("}");
      _builder_8.newLine();
      AyinEntity _parse = this._parseHelper.parse(_builder_8, resourceSet);
      return ((AyinExecutor) _parse);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void callbackBlock() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("i32 x = 0;");
    _builder.newLine();
    _builder.append("i32 y = 0;");
    _builder.newLine();
    _builder.append("i32 z = 0;");
    _builder.newLine();
    _builder.append("this.test(msg = \"test\", test1 = {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("i32 x = 0;");
    _builder.newLine();
    _builder.append("});");
    _builder.newLine();
    _builder.newLine();
    _builder.append("this.test(msg = \"test\", test1 = {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("i32 a = x;");
    _builder.newLine();
    _builder.append("});");
    _builder.newLine();
    this._validationTestHelper.assertError(this.parseExecutor(_builder), 
      AyinPackage.eINSTANCE.getAyinParameter(), 
      AyinValidator.DUPLICATED_PARAMETER, 
      "duplicated parameter");
  }
  
  @Test
  public void stringType() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("string s = \"test string\";");
    _builder.newLine();
    this._validationTestHelper.assertNoErrors(this.parseExecutor(_builder));
  }
  
  @Test
  public void integerTypes() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("i8 x1 = 125;");
    _builder.newLine();
    _builder.append("i16 x2 = 250;");
    _builder.newLine();
    _builder.append("i32 x3 = 50000;");
    _builder.newLine();
    _builder.append("i64 x4 = 1000000000;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("x4 = x4;");
    _builder.newLine();
    _builder.append("x4 = x3;");
    _builder.newLine();
    _builder.append("x4 = x2;");
    _builder.newLine();
    _builder.append("x4 = x1;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("x3 = x3;");
    _builder.newLine();
    _builder.append("x3 = x2;");
    _builder.newLine();
    _builder.append("x3 = x1;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("x2 = x2;");
    _builder.newLine();
    _builder.append("x2 = x1;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("x1 = x1;");
    _builder.newLine();
    this._validationTestHelper.assertNoErrors(this.parseExecutor(_builder));
  }
  
  @Test
  public void integerTypesErrors() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("i8 x1 = 125;");
    _builder.newLine();
    _builder.append("i16 x2 = 250;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("x1 = x2;");
    _builder.newLine();
    this._validationTestHelper.assertError(this.parseExecutor(_builder), 
      AyinPackage.eINSTANCE.getAyinAssignment(), 
      AyinValidator.INCOMPATIBLE_TYPES, 
      "expected type \'i8\' but got \'i16\'");
  }
  
  @Test
  public void dataTypes() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("List<Pin> p = new List<Pin>();");
    this._validationTestHelper.assertNoErrors(this.parseExecutor(_builder));
  }
  
  @Test
  public void dataAntiTypes() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("List<Pin> p = new List<Nip>();");
    this._validationTestHelper.assertError(this.parseExecutor(_builder), 
      AyinPackage.eINSTANCE.getAyinAssignment(), 
      AyinValidator.INCOMPATIBLE_TYPES, 
      "expected type \'List<Pin>\' but got \'List<Nip>\'");
  }
  
  @Test
  public void structTypes() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Pin p = new Pin();");
    _builder.newLine();
    _builder.append("p.p = 3;");
    _builder.newLine();
    this._validationTestHelper.assertNoErrors(this.parseExecutor(_builder));
  }
  
  @Test
  public void assignmentSize() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("i8 x, i8 y = 0");
    this._validationTestHelper.assertError(this.parseExecutor(_builder), 
      AyinPackage.eINSTANCE.getAyinAssignment(), 
      AyinValidator.ASSIGNMENT_SIZES_ERROR, 
      ("number of elements on the left side of assignment" + 
        " is not equal to number of elements on the right side"));
  }
  
  @Test
  public void nonParentableInstantiation() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("IBase b = new IBase();");
    this._validationTestHelper.assertError(this.parseExecutor(_builder), 
      AyinPackage.eINSTANCE.getAyinNewOperator(), 
      AyinValidator.NON_PARENTABLE_INSTANTIATION, 
      "it is not allowed to instantiate AyinInterface");
  }
  
  @Test
  public void nonExceptionInThrow() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("throw new Pin();");
    this._validationTestHelper.assertError(this.parseExecutor(_builder), 
      AyinPackage.eINSTANCE.getAyinThrow(), 
      AyinValidator.NON_EXCEPTION_IN_THROW, 
      "expected expression of type \'AyinException\', but got \'AyinStruct\'");
  }
  
  @Test
  public void covarianceOfStruct() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Pin p = new SuperPin();");
    this._validationTestHelper.assertNoErrors(this.parseExecutor(_builder));
  }
}
